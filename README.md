**This is the home page of [this documentation](http://wonderland.eode.studio/docs/inputs-_introduction.html)**

## Introduction

### Version
> Low API Released, stable

> Requires a HLAPI

### Dependencies
- `Common`

## Motivation
`Inputs` is a set of classes and interfaces. It is not a download and use plugin, you must consider what you want to implement for your users or your skills.

### Joysticks management
In the main menu of Wonderland, there are helpers to manage joystick axis.

The component JoysticksViewer lists and configures all gamepads. You can change the dead zone, auto sleep unused gamepads and reduce axis angle (X8, X16, useful for network or specific 2d games).

![joysticksviewer](http://wonderland.eode.studio/Inputs/joysticksviewer.png)

### HardKeys <=> ActionKeys
Transforms controllers keys in your custom Action enum.

```C#
public enum ActionType {
    Unknown,
    Main,
    Secondary,
    OtherAction
}
```

### Create your own hardware
Create custom types of joysticks (networked, AI...)

### Link players with controllers
Add, remove or switch Game players and Controllers.

## Implementation Example
I know it's hard to understand. It's a bonus, just go to next page.

![hardexample](http://wonderland.eode.studio/Inputs/hardexample.png)