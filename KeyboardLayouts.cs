﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace EODE.Wonderland {
    public static class KeyboardLayouts {
        [RuntimeInitializeOnLoadMethod]
        static void KeyboardLayoutsInit() {
            KeyboardInfos.GetCurrentKeyboardLayout = GetCurrentKeyboardLayout;
            KeyboardInfos.UpdateLayout();
        }

#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
        static readonly int[] _azertyLayouts = new int[] {
            0x0000040c,
            2060,
            1156,
            6156,
            6145,
            5121,
            7169
        };

        static readonly int[] _qwertzLayouts = new int[] {
            0x00000407,
            0x00010407,
            0x0000041c,
            3079,
            0x0001042e,
            0x0002042e,
            0x0000042e,
            0x00000405,
            0x0000040e,
            0x0001040e,
            0x00010415,
            0x00000415,
            0x00000418,
            0x00020418,
            0x00010418,
            0x0000041b,
            0x0000041a,
            0x0000081a,
            0x00000424,
            0x0000100c,
            0x00000807,
            5127,
            4103,
            2094,
            4122,
            5132
        };

        [System.Runtime.InteropServices.DllImport("user32.dll")] static extern IntPtr GetForegroundWindow();
        [System.Runtime.InteropServices.DllImport("user32.dll")] static extern uint GetWindowThreadProcessId(IntPtr hwnd, IntPtr proccess);
        [System.Runtime.InteropServices.DllImport("user32.dll")] static extern IntPtr GetKeyboardLayout(uint thread);
        static KeyboardInfos.KeyboardLayout GetCurrentKeyboardLayout() {
            try {
                IntPtr foregroundWindow = GetForegroundWindow();
                uint foregroundProcess = GetWindowThreadProcessId(foregroundWindow, IntPtr.Zero);
                int keyboardLayout = GetKeyboardLayout(foregroundProcess).ToInt32() & 0xFFFF;

                if (_azertyLayouts.Contains(keyboardLayout)) {
                    return KeyboardInfos.KeyboardLayout.AZERTY;
                }
                else if (_qwertzLayouts.Contains(keyboardLayout)) {
                    return KeyboardInfos.KeyboardLayout.QWERTZ;
                }

                return KeyboardInfos.KeyboardLayout.QWERTY;
            }
            catch {
                return KeyboardInfos.KeyboardLayout.QWERTY;
            }
        }
#elif UNITY_STANDALONE_LINUX || UNITY_EDITOR_LINUX
        static KeyboardInfos.KeyboardLayout GetCurrentKeyboardLayout() {
            return KeyboardInfos.KeyboardLayout.QWERTY;
        }
#elif UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
        static KeyboardInfos.KeyboardLayout GetCurrentKeyboardLayout() {
            return KeyboardInfos.KeyboardLayout.QWERTY;
        }
#else
        static KeyboardInfos.KeyboardLayout GetCurrentKeyboardLayout() {
            return KeyboardInfos.KeyboardLayout.QWERTY;
        }
#endif
    }
}